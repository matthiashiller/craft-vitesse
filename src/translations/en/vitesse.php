<?php
/**
 * Vitesse plugin for Craft CMS 3.x
 *
 * Debundle vitejs manifest
 *
 * @link      https://hiller.digital
 * @copyright Copyright (c) 2021 Matthias Hiller
 */

/**
 * Vitesse en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('vitesse', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Matthias Hiller
 * @package   Vitesse
 * @since     0.0.1
 */
return [
    'Vitesse plugin loaded' => 'Vitesse plugin loaded',
];
