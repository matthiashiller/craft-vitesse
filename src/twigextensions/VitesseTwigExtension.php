<?php
/**
 * Vitesse plugin for Craft CMS 3.x
 *
 * Debundle vitejs manifest
 *
 * @link      https://hiller.digital
 * @copyright Copyright (c) 2021 Matthias Hiller
 */

namespace hillerdigital\vitesse\twigextensions;

use hillerdigital\vitesse\Vitesse;
use Craft;
use craft\helpers\FileHelper;


use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Twig\Markup;

/**
 * Twig can be extended in many ways; you can add extra tags, filters, tests, operators,
 * global variables, and functions. You can even extend the parser itself with
 * node visitors.
 *
 * http://twig.sensiolabs.org/doc/advanced.html
 *
 * @author    Matthias Hiller
 * @package   Vitesse
 * @since     0.0.1
 */
class VitesseTwigExtension extends AbstractExtension
{
    public $settings;
    // Public Methods
    // =========================================================================

    public function __construct()
    {
        $this->settings = Vitesse::$plugin->getSettings();
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName(): string
    {
        return 'Vitesse';
    }


    /**
     * Returns an array of Twig functions, used in Twig templates via:
     *
     *      {% set this = someFunction('something') %}
     *
     * @return array
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('vite', [$this, 'vite']),
            new TwigFunction('viteCSS', [$this, 'viteCSS']),
            new TwigFunction('viteJS', [$this, 'viteJS']),
            new TwigFunction('viteSvgSprite', [$this, 'viteSvgSprite']),
        ];
    }

    /**
     * @param string $entryJS
     *
     * @return mixed
     */
    public function vite(string $entryJS = 'main')
    {
        $this->settings->setViteEnv($entryJS);
        return $this->settings->getScriptsStyles();
    }

    /**
     * @param string $entryJS
     *
     * @return Markup
     */
    public function viteCSS(string $entryJS = 'main'): Markup
    {
        $this->settings->setViteEnv($entryJS);
        return $this->settings->getStyles();
    }

    /**
     * @param string $entryJS
     *
     * @return Markup
     */
    public function viteJS(string $entryJS = 'main'): Markup
    {
        $this->settings->setViteEnv($entryJS);
        return $this->settings->getScripts();
    }

    /**
     * @param string $file
     *
     * @return string
     */
    public function viteSvgSprite(string $file): string
    {
        $pubicPath = $this->settings->publicPath;
        $found = FileHelper::findFiles(Craft::getAlias('@webroot').$pubicPath, [
            'recursive' => false,
            'only' => [$file.'.*.svg']
        ]);
        if ($found) {
            return $pubicPath.basename($found[0]);
        }
        return '';
    }
}
