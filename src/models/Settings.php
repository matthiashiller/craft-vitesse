<?php
/**
 * Vitesse plugin for Craft CMS 3.x
 *
 * Debundle vitejs manifest
 *
 * @link      https://hiller.digital
 * @copyright Copyright (c) 2021 Matthias Hiller
 */

namespace hillerdigital\vitesse\models;

use craft\helpers\Html;
use craft\helpers\Template;

use Craft;
use craft\base\Model;
use Twig\Markup;

/**
 * Settings Model
 *
 * Models are containers for data. Just about every time information is passed
 * between services, controllers, and templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 *
 * @author    Matthias Hiller
 * @package   Vitesse
 * @since     0.0.1
 */
class Settings extends Model
{
    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $publicPath = '/dist/';

    /**
     * @var string
     */
    public $sourcePath = '/src/';

    /**
     * @var bool
     */
    public $enable = false;

    /**
     * @var bool
     */
    public $isLocal = false;

    /**
     * @var string
     */
    public $protocol = 'http';

    /**
     * @var string
     */
    public $host = 'localhost';

    /**
     * @var int
     */
    public $port = 3000;

    /**
     * @var string
     */
    public $devServer;

    /**
     * @var array|mixed
     */
    public $manifest = [];


    /**
     * @var string
     */
    protected $devScript = '';

    /**
     * @var string
     */
    protected $key = '';


    // Public Methods
    // =========================================================================

    /**
     * @return Markup
     */
    public function getStyles(): Markup
    {
        if ($this->enable && $this->isLocal) {
            $url = $this->devServer.'/@vite/client';
            $script = Html::jsFile($url, [
                'type' => 'module',
            ]);
            return Template::raw($script);
        }

        return $this->_renderCSS();
    }

    /**
     * @return Markup
     */
    public function getScripts(): Markup
    {
        if ($this->enable && $this->isLocal) {
            $script = Html::jsFile($this->devScript, [
                'type' => 'module',
            ]);
            return Template::raw($script);
        }

        return $this->_renderJS();
    }

    public function getScriptsStyles()
    {
        return Template::raw($this->getStyles().$this->getScripts());
    }

    /**
     * @param $entryJS
     */
    public function setViteEnv($entryJS): void
    {
        $this->devServer = $this->getDevServer();
        $this->devScript = $this->devServer.$this->sourcePath.$entryJS.'.js';
        $this->key = ltrim($this->sourcePath, '/').$entryJS.'.js';
        if (!$this->isLocal || ($this->isLocal && !$this->enable)) {
            $this->manifest = $this->_setManifest();
        }
    }

    /**
     * @return string
     */
    public function getDevServer(): string
    {
        return $this->protocol.'://'.$this->host.':'.$this->port;
    }


    // Private Methods
    // =========================================================================

    /**
     * @return mixed
     */
    private function _setManifest()
    {
        $path = Craft::getAlias('@webroot').$this->publicPath.'manifest.json';
        try {
            return json_decode(file_get_contents($path), true);
        } catch (\Exception $e) {
            Craft::error($path.' could not convert to array: '.$e->getMessage());
            $this->manifest = [];
        }

    }

    /**
     * @return Markup
     */
    private function _renderCSS(): Markup
    {
        $markup = '';
        foreach ($this->manifest[$this->key]['css'] as $css) {
            $markup .= Html::cssFile($this->publicPath.$css);
        }

        return Template::raw($markup);
    }

    /**
     * @return Markup
     */
    private function _renderJS(): Markup
    {

        $markup = Html::jsFile($this->publicPath.$this->manifest[$this->key]['file'], [
            'type' => 'module',
        ]);
        if (isset($this->manifest[$this->key]['imports'])) {
            foreach ($this->manifest[$this->key]['imports'] as $import) {
                $file = $this->manifest[$import]['file'];
                $markup .= Html::jsFile($this->publicPath.$file, [
                    'type' => 'module',
                ]);
            }
        }

        return Template::raw($markup);
    }

}
